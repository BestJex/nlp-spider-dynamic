package org.wisdomdata.framework;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.wisdomdata.selenium.processor.SeleniumJumpLoopProcessor;

public class RootProcessor implements Processor{
	private final static Logger logger =
			Logger.getLogger(SeleniumJumpLoopProcessor.class.getName());
	@Autowired
	private TaskCommon taskCommon;
	
	public TaskCommon getTaskCommon() {
		return taskCommon;
	}
	public void setTaskCommon(TaskCommon taskCommon) {
		this.taskCommon = taskCommon;
	}
	
	
	private List<Processor> childProcessors;
	public List<Processor> getChildProcessors() {
		return childProcessors;
	}

	public void setChildProcessors(List<Processor> childProcessors) {
		this.childProcessors = childProcessors;
	}

	public boolean prepareProcess() {
		return true;
	}

	public void process() {
		logger.info("RootProcessor: root process begin run...");
		if (prepareProcess()) {
			innerProcess();
			quitProcess();
		}
		logger.info("RootProcessor: root process run end...");
	}

	public void innerProcess() {
		if (this.getChildProcessors() != null && this.getChildProcessors().size() > 0) {
			logger.info("RootProcessor: begin to deal with the child processor...");
			for (Processor processor : this.getChildProcessors()) {
				processor.process();
			}	
		}
	}
	public void quitProcess() {
		this.taskCommon.writeTables();
	}
	
	
}
