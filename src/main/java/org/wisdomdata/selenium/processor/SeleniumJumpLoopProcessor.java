package org.wisdomdata.selenium.processor;

import java.util.logging.Logger;

import org.wisdomdata.framework.Processor;


public class SeleniumJumpLoopProcessor extends SeleniumActionProcessor {
	private final static Logger logger =
			Logger.getLogger(SeleniumJumpLoopProcessor.class.getName());
	
	public void innerProcess() {
		this.commonExtract();
		if (this.getChildProcessors() != null && this.getChildProcessors().size() > 0) {
			logger.info("begin to deal with the child processor...");
			for (Processor processor : this.getChildProcessors()) {
				//循环执行下一个页面的时候，先跳转
				
				this.doAction();

				processor.process();
				
			}	
		}
	}
	
	
}
