package org.wisdomdata.selenium.processor;

import java.util.List;
import java.util.logging.Logger;

import org.wisdomdata.framework.Action;
import org.wisdomdata.selenium.SeleniumProcessor;


public class SeleniumLoginProcessor extends SeleniumProcessor {
	private final static Logger logger =
			Logger.getLogger(SeleniumLoginProcessor.class.getName());
	private List<Action> actions;
	private long waitTime;

	@Override 
	public boolean prepareProcess() {
		if (this.getSearchContext() == null) {
			logger.warning("the selenium must init web driver, please check it!");
			return false;
		} 
		return true;
	}
	public void innerProcess() {
		try {
			Thread.sleep(this.getWaitTime());
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		for (Action action : actions) {
			try {
				action.action();
			} catch (Exception e) {
				logger.info("there must be some thing can not locate, please input by yourself, wait " + this.getWaitTime() + " minutes...");
				try {
					Thread.sleep(this.getWaitTime());
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		
	}

	@Override
	public void quitProcess() {
	}
	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public long getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(long waitTime) {
		this.waitTime = waitTime;
	}

	

}
