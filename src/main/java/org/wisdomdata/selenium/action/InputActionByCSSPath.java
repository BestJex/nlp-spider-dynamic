package org.wisdomdata.selenium.action;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumAction;

public class InputActionByCSSPath extends SeleniumAction {
	private String inputString;
	
	public String getInputString() {
		return inputString;
	}
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}


	private String csspath;
	public String getCsspath() {
		return csspath;
	}
	public void setCsspath(String csspath) {
		this.csspath = csspath;
	}


	public void action() throws NoSuchElementException{
		WebElement saveButton = this.getSearchContext().findElement(
					By.cssSelector(getCsspath())
				);
		saveButton.sendKeys(this.getInputString());
	}

}
