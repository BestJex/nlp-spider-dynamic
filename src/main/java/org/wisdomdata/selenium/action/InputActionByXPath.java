package org.wisdomdata.selenium.action;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumAction;

public class InputActionByXPath extends SeleniumAction {
	private String inputString;
	
	public String getInputString() {
		return inputString;
	}
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}


	private String xpath;
	public String getXpath() {
		return xpath;
	}
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}
	
	
	public void action() throws NoSuchElementException{
		WebElement saveButton = this.getSearchContext().findElement(
					By.xpath(this.getXpath())
				);
		saveButton.sendKeys(this.getInputString());
	}

}
