package org.wisdomdata.selenium.action;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumAction;

public class JumpActionByXPath extends SeleniumAction {
	private String xpath;
	public String getXpath() {
		return xpath;
	}

	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public void action() throws NoSuchElementException{
		WebElement saveButton = this.getParentSearchContext().findElement(
					By.xpath(getXpath())
				);
		//跳转之前记录一下之前的链接，以便能够返回
		
		String newUri = saveButton.getAttribute("href");
		((WebDriver) this.getSearchContext()).get(newUri);
		
	}

}
