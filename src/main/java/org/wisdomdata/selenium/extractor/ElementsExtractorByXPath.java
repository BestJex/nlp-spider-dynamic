package org.wisdomdata.selenium.extractor;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumElementsExtractor;

public class ElementsExtractorByXPath extends SeleniumElementsExtractor{
	private static final Logger logger = 
			Logger.getLogger(ElementsExtractorByXPath.class.getName());
	private String xpath;
	public String getXpath() {
		return xpath;
	}
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public void innerExtract() throws NoSuchElementException {
		List<WebElement> results = new ArrayList<WebElement>();
		List<WebElement> elements = this.getSearchContext().findElements(By.xpath(getXpath()));
		
		if (this.isNeedAll() == true) {
			this.setBeginIndex(0);
			this.setInterval(1);
			this.setEndIndex(elements.size() - 1);
		}
		List<Integer> positions = this.parsePosition();
		if (positions != null) {
			for (int index : positions) {
				WebElement e = elements.get(index);
				if (e == null) {
					logger.warning("there do not have so many elements you want!");
					break;
				} else {
					results.add(e);
				}
			}
		}
		this.setExtractResults(results);
	}

	
	
}
