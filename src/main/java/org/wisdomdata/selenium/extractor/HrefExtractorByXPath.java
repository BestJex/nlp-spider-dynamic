package org.wisdomdata.selenium.extractor;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumStringExtractor;

public class HrefExtractorByXPath extends SeleniumStringExtractor{
	private String xpath;
	public String getXpath() {
		return xpath;
	}
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	/**
	 * 目标抽取函数，如果不存在这个元素，或者不存在href属性就会返回NULL
	 * */
	public void innerExtract() throws NoSuchElementException{
		
		String result = "";
		WebElement element = this.getSearchContext().findElement(By.xpath(getXpath()));
		result = element.getAttribute("href");
		this.setExtractResult(result);
	}

}
