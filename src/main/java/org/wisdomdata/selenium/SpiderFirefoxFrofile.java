package org.wisdomdata.selenium;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.openqa.selenium.firefox.FirefoxProfile;

public class SpiderFirefoxFrofile extends FirefoxProfile {
	private Map<String, Integer> intArguments;
	public Map<String, Integer> getIntArguments() {
		return intArguments;
	}
	public void setIntArguments(Map<String, Integer> intArguments) {
		setArguments(intArguments);
		this.intArguments = intArguments;
		
	}
	
	private Map<String, Boolean> booleanArguments;
	public Map<String, Boolean> getBooleanArguments() {
		return booleanArguments;
	}
	public void setBooleanArguments(Map<String, Boolean> booleanArguments) {
		setArguments(intArguments);
		this.booleanArguments = booleanArguments;
	}
	
	private Map<String, String> stringArguments;
	
	public Map<String, String> getStringArguments() {
		return stringArguments;
	}
	public void setStringArguments(Map<String, String> stringArguments) {
		setArguments(intArguments);
		this.stringArguments = stringArguments;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setArguments(Map intArguments) {
		Set<Entry> set = intArguments.entrySet();
		Iterator<Entry> it = set.iterator();
		while (it.hasNext()) {
			Entry<String, Integer> en = it.next();
			this.setPreference(en.getKey(), en.getValue());
		}
	}
}
